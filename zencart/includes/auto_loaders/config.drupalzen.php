<?php

/**
 * autoloader array for catalog application_top.php for Drupal/Zencart Integration
 *
 * @package initSystem
 * @copyright Copyright 2007 Gorton Studios (http://www.gortontstudios.com)
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 */
if (!defined('IS_ADMIN_FLAG')) {
 die('Illegal Access');
} 

$autoLoadConfig[][] = array('autoType'=>'class',
                                'loadFile'=>'observers/drupalzen_observer.php');
$autoLoadConfig[][] = array('autoType'=>'classInstantiate',
                               'className'=>'drupalzen_observer',
                               'objectName'=>'drupalzen_observer');
?>