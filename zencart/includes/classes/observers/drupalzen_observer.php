<?php

/** 
 * Oberver class for Drupal/Zencart Integration
 *
 * @package classes
 * @copyright Copyright 2007 Gorton Studios (http://www.gortontstudios.com)
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 */

/**
 * drupalzen_observer
 *
 * Oberver class for Drupal/Zencart User Integration
 *
 * @package classes
 */
 
 
class drupalzen_observer extends base {
  
  function drupalzen_observer() {
    global $zco_notifier;
    $zco_notifier->attach($this, 
      array(
        'NOTIFY_HEADER_START_LOGIN',
        'NOTIFY_HEADER_START_EZPAGE',
        'NOTIFY_HEADER_START_PRODUCT_INFO',
        'NOTIFY_HEADER_START_ADVANCED_SEARCH_RESULTS',
        'NOTIFY_HEADER_START_INDEX',
        'NOTIFY_HEADER_START_PRODUCT_REVIEWS_WRITE', 
        'NOTIFY_HEADER_START_CHECKOUT_SHIPPING',
        'NOTIFY_HEADER_START_CHECKOUT_PAYMENT',
        )
      );  
    
    $pages = array(
      'homepage',
      'privacy', 
      'shippinginfo',
      'conditions',
      'contact_us',
      'site_map',
      'gv_faq',
      'discount_coupon',
      'unsubscribe',
      'products_all',
      'specials',
      'products_new',
      'featured_products',
      'page_2',
      'page_3',
      'page_4',
      'advanced_search',
      'cookie_usage',
      'ssl_check',
      'time_out',
      'down_for_maintenance',
      'download_timeout',
    );
    if( in_array( $_REQUEST['main_page'], $pages ) ) {
      $const = 'DRUPAL_REDIRECT_'.strtoupper( $_REQUEST['main_page'] );
      if( defined( $const ) && $url = constant( $const ) ) {
        zen_redirect($url);
      }
    }
  }
  
  function update(&$callingClass, $notifier, $paramsArray) {
    global $cPath_array;
    switch ( $notifier ) {
      case "NOTIFY_HEADER_START_INDEX":
        // redirect the category listing pages
        if( defined( 'DRUPAL_REDIRECT_CATEGORY_LISTING' ) && DRUPAL_REDIRECT_CATEGORY_LISTING && DRUPAL_HREF && $cPath_array ) {
          $category_id = (int)array_pop( $cPath_array );
          zen_redirect(DRUPAL_HREF."?q=zencart/category/".$category_id);
        }
        // redirect the zencart homepage
        else if ( 
          defined( 'DRUPAL_REDIRECT_HOMEPAGE' ) && DRUPAL_REDIRECT_HOMEPAGE &&
          (!isset($_GET['manufacturers_id'])  || (int)$_GET['manufacturers_id'] <= 0) &&
          (!isset($_GET['music_genre_id'])    || (int)$_GET['music_genre_id'] <= 0) &&
          (!isset($_GET['record_company_id']) || (int)$_GET['record_company_id'] <= 0)
        ) {
          zen_redirect(DRUPAL_REDIRECT_HOMEPAGE);
        }
      break;
      case "NOTIFY_HEADER_START_PRODUCT_INFO":
        if( defined( 'DRUPAL_REDIRECT_PRODUCT_INFO' ) && DRUPAL_REDIRECT_PRODUCT_INFO && DRUPAL_HREF ) {
          zen_redirect(DRUPAL_HREF."?q=zencart/product/".(int)$_GET['products_id']);
        }
      break;
      case "NOTIFY_HEADER_START_EZPAGE":
        
      break;
      case "NOTIFY_HEADER_START_LOGIN":        
        if( defined( 'DRUPAL_SINGLE_SIGNON' ) && DRUPAL_SINGLE_SIGNON ) {
          if (sizeof($_SESSION['navigation']->snapshot) > 0) {
            $origin_href = zen_href_link(
                                $_SESSION['navigation']->snapshot['page'], 
                                zen_array_to_string($_SESSION['navigation']->snapshot['get'], 
                                array(zen_session_name())), 
                                $_SESSION['navigation']->snapshot['mode']
                              );
            $origin_href = "zencart/redirect/". $_SESSION['navigation']->snapshot['page'];
          }
          
          if( $origin_href ) {
            zen_redirect(DRUPAL_HREF."?q=user&destination=".urlencode($origin_href));
          } else {
            zen_redirect(DRUPAL_HREF."?q=user");
          }
        }
        break;
      case 'NOTIFY_HEADER_START_CHECKOUT_SHIPPING':
        if( defined( 'DRUPAL_SINGLE_SIGNON' ) && DRUPAL_SINGLE_SIGNON && !$_SESSION['customer_default_address_id'] && !$_SESSION['sendto'] ) {
          zen_redirect( zen_href_link( 'checkout_shipping_address' ) );
        } else if ( $_SESSION['sendto'] ) { 
          $this->setPrimaryAddress( $_SESSION['sendto'] );
        }
      break;
      case 'NOTIFY_HEADER_START_CHECKOUT_PAYMENT':
        if( defined( 'DRUPAL_SINGLE_SIGNON' ) && DRUPAL_SINGLE_SIGNON && !$_SESSION['customer_default_address_id'] && !$_SESSION['billto'] && !$_SESSION['sendto'] ) {
          zen_redirect( zen_href_link( 'checkout_billing_address' ) );
        } else if ( $_SESSION['billto'] ) {
          $this->setPrimaryAddress( $_SESSION['billto'] );
        }
      break;
    }
  }
  
  function setPrimaryAddress( $address_id ) {
    global $db;
    
    $address_query = "select entry_firstname as firstname, entry_lastname as lastname,
                       entry_company as company, entry_street_address as street_address,
                       entry_suburb as suburb, entry_city as city, entry_postcode as postcode,
                       entry_state as state, entry_zone_id as zone_id,
                       entry_country_id as country_id
                from " . TABLE_ADDRESS_BOOK . "
                where customers_id = '" . (int)$_SESSION['sendto'] . "'
                and address_book_id = '" . (int)$_SESSION['customer_id'] . "'";
    
    $address = $db->Execute($address_query);
    
    $_SESSION['customer_default_address_id'] = $address_id;  
    $_SESSION['customer_first_name'] = $address->fiels['firstname'];
    $_SESSION['customer_country_id'] = $address->fiels['country'];
    $_SESSION['customer_zone_id'] = (($address->fiels['zone_id'] > 0) ? (int)$address->fiels['zone_id'] : '0');

    $sql_data_array = array(array('fieldName'=>'customers_firstname', 'value'=>$address->fiels['firstname'], 'type'=>'string'),
                            array('fieldName'=>'customers_lastname', 'value'=>$address->fiels['country'], 'type'=>'string'),
                            array('fieldName'=>'customers_default_address_id', 'value'=>$address_id, 'type'=>'integer'));

    if (ACCOUNT_GENDER == 'true') $sql_data_array[] = array('fieldName'=>'customers_gender', 'value'=>$address->fiels['gender'], 'type'=>'enum:m|f');
    $where_clause = "customers_id = :customersID";
    $where_clause = $db->bindVars($where_clause, ':customersID', $_SESSION['customer_id'], 'integer');
    $db->perform(TABLE_CUSTOMERS, $sql_data_array, 'update', $where_clause);
  }
}  
?>