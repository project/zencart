<?php
/******************************************************************************
 *
 *  File:      zencart_users.module
 *
 *  Description:  Zen Cart integration for drupal
 *
 *  Author(s):    Ronan Dowling
 *
 *  Created:    7/17/06
 *
 *  Copyright:    Copyright (c) 2006 Gortons.net
 *          All Rights Reserved.
 *
 *  Notes:      
 *  
 *****************************************************************************/

/**
 * Implementation of hook_help
 */
function zencart_users_users_help($section) {
  $output = '';
   
  return $output;
}


// Implementation of hook_perm().
function zencart_users_perm() {
  return array('administer zencart users');
}

/**
 * implementation of hook_zencart
 */
function zencart_users_zencart($op, $node = NULL) {
  $form = array();
  switch ($op) {
    case 'settings':
      return zencart_users_admin_settings();
    break;
    case 'config':
      return _zencart_users_default_config();
    break;
   }
}

/**
 * Get the settings requirements for Zen Cart users
 */
function _zencart_users_default_config() {
  return  
    array(
      "DRUPAL_SINGLE_SIGNON" => array(
        "value"       => variable_get('zen_allow_sso', false) ? 1 : 0,
        "group"       => NULL,
        "name"        => "Allow Drupal/Zencart Single Signon",
        "description" => "Allow single signon between Zen Cart and Drupal. This is set automatically and should not be changed.",
        "required"    => true
      ),
    );
}

function zencart_users_admin_settings() {
  // only administrators can access this page
  if (!user_access('administer zencart users'))
    return message_access();
  
  $form = array();
  $form['zencart_users'] = array('#type' => 'fieldset', 
    '#title' => t('Zen Cart Users'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE);
  
  $form['zencart_users']['zen_customer_drupal_login'] = 
    array('#type' => 'checkbox', 
      '#title' => t('Allow Zen Cart Customers to login to Drupal'), 
      '#default_value' => variable_get('zen_customer_drupal_login', true), 
      '#description' => t('Allow users with existing Zen Cart customer accounts to login to drupal.')
    ); 
  $form['zencart_users']['zen_drupal_customer_login'] = 
    array('#type' => 'checkbox', 
      '#title' => t('Allow Drupal Users Customers to login to Zen Cart as Customers'), 
      '#default_value' => variable_get('zen_drupal_customer_login', true), 
      '#description' => t('If this option is set a Zen Cart Customer account will be created for Drupal users upon next login.')
    );
    
  /*
  $form['zencart_users']['zen_allow_admin_login'] = 
    array('#type' => 'checkbox', 
      '#title' => t('Allow Zen Cart Admins to login to Drupal'), 
      '#default_value' => variable_get('zen_allow_admin_login', true), 
        '#description' => t('Allow users with existing Zen Cart admin accounts to login to drupal.')
    );
  */
  
  $form['zencart_users']['zen_allow_sso'] = 
    array('#type' => 'checkbox', 
      '#title' => t('Allow Single Sign-On'), 
      '#default_value' => variable_get('zen_allow_sso', false), 
      '#description' => t('Single Sign-On allows users to login once and access both Drupal and Zen Cart.')
    );
  
  return $form;
}

/**
 * Implementation of hook_form_alter()
 */
function zencart_users_form_alter($form_id, &$form) {
  global $form_values;
  switch ($form_id) {
    case 'user_register':
      break;
    case 'user_edit':
      break;
    case 'user_login':
    case 'user_login_block':
      if (variable_get('zen_customer_drupal_login', true)) {
        $form['name']['#title'] = t('Username or e-mail address');
        if (variable_get('drupal_authentication_service', FALSE) && count(user_auth_help_links()) > 0) {
          $form['name']['#description'] = t('Enter your %s username, e-mail address, or an ID from one of our affiliates: !a.', array('%s' => variable_get('site_name', 'local'), '!a' => implode(', ', user_auth_help_links())));
        } else {
          $form['name']['#description'] = t('Enter your %s username or your e-mail address.', array('%s' => variable_get('site_name', 'local')));
        }
      }
      break;
  }
}

/**
 * Implementation of hook_init
 */
function zencart_users_init() {
  global $user;
  
  // If single signon is active, log the user in or out of Drupal 
  // if they are logged in our out of Zen Cart
  if( variable_get('zen_allow_sso', false) ) {
    
    // update the session expiry to prevent logout if user is active in drupal
    _zencart_session_touch();
    
    $check_customer_query = "SELECT customers_email_address
                            FROM " . TABLE_CUSTOMERS . "
                            WHERE customers_id = '%d'";
    
    // if the user has logged into Zen Cart but not into Drupal
    if( variable_get('zen_customer_drupal_login', true) && !$user->uid && $customer_id = _zencart_session_read( "customer_id" ) ) {
      // See if the user exists in Zen Cart
      $customer = db_fetch_object( _zencart_db_query( $check_customer_query, $customer_id ));      
      if( $email = $customer->customers_email_address ) {
        // Find the user in the Drupal user table
        $account = user_load(array('mail' => $email));
        // Register this new user.
        if (!$account->uid) { 
          $user = drupal_anonymous_user();
          $userinfo = array(  
            'name' => $email, 
            'pass' => user_password(), 
            'init' => $email, 
            'mail' => $email, 
            'status' => 1, 
            'zencart_customers_id' => $customer_id,
            'authname_zencart_users' => $email,
          );
          $user = user_save('', $userinfo);
          
          // login this new user
          user_module_invoke('login', $form_values, $user);
          sess_regenerate();
          watchdog('user', t('New external user: %user using module %module.', array('%user' => $name, '%module' => "zencart_users")), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $user->uid .'/edit'));
        } 
        // if the user has not been disabled set as the current user
        else if ( $account && $account->status ) {
          $user = $account;
          $message = t('Session opened by Zen Cart Single Signon for %name.', array('%name' => $user->name));
          sess_regenerate();
        }
      }
    } 
    // A Zen user is logged out of Zen Cart, but is logged into Drupal
    else if ( $user->uid && $user->zencart_customers_id && $user->zencart_customers_id !== _zencart_session_read( "customer_id" ) ) {
//      die( 'attempting to logout drupal user becuase '.$user->zencart_customers_id.'!=='._zencart_session_read( "customer_id" ) );
      
      // make sure the Zen customer exists
      $customer = db_fetch_object( _zencart_db_query( $check_customer_query, $user->zencart_customers_id ));
      if( $customer ) {
        // Destroy the current session:
        session_destroy();
        module_invoke_all('user', 'logout', NULL, $user);
        
        // Load the anonymous user
        $user = drupal_anonymous_user();
      } 
      else {
        // customer_id does not correspond to an existing Zen Cart customer, so get rid of it
        user_save( $user, array( 'zencart_customers_id' => NULL, 'zencart_customer_generated' => NULL ) );
      }
    }
  }
}


/**
 * Implementation of hook_user
 */
function zencart_users_user($op, &$edit, &$user_edit, $category = NULL) {
  // log the user out of zencart
  switch( $op ) {
    case "login":
      if( variable_get('zen_drupal_customer_login', true ) && !$user_edit->zencart_customers_id ) {
        $zencart_customers_id = _zencart_create_zencart_customer( $user_edit->mail , $edit['pass'] );
        if( $zencart_customers_id ) {
          $user_edit = user_save($user_edit, array("zencart_customers_id"=>$zencart_customers_id,"zencart_customer_generated"=>true));
        }
      }
      if( variable_get('zen_allow_sso', false) && $user_edit->zencart_customers_id ) {
        _zencart_users_login_to_zencart( $user_edit->zencart_customers_id ); 
      }
      
    break;
    case "logout":
      _zencart_session_destroy();
    break;
    case "insert":
      // if the user init is an email, check if it's a Zen user
      if( variable_get('zen_allow_sso', false) && strpos( @$user_edit->init, "@" ) ) {
        $check_customer_query = 
                            "SELECT customers_firstname, customers_lastname, customers_id
                              FROM " . TABLE_CUSTOMERS . "
                              WHERE customers_email_address = '%s'";    
        $customer = db_fetch_object( _zencart_db_query( $check_customer_query, $user_edit->init ));
        if( $customer->customers_id ) {
          $edit['zencart_customers_id'] = $customer->customers_id;
          // generate a username so that users are not listed publicly by their email address
          $username = _zencart_users_get_username( $customer->customers_firstname, $customer->customers_lastname, $user_edit->init );
          db_query("UPDATE {users} SET mail = '%s', name = '%s' WHERE uid = %d", $user_edit->init, $username, $user_edit->uid);
        }
      }
    break;
    case "update":
      // if the user has a zencart account, make sure we update the info there.
      if( variable_get('zen_allow_sso', false) &&  $user_edit->zencart_customers_id ) {
        $sql = "UPDATE ". TABLE_CUSTOMERS . "
                   SET customers_email_address = '%s'
                 WHERE customers_id = %d";
        _zencart_db_query( $sql, $edit['mail'], $user_edit->zencart_customers_id );
        
        if( $edit['pass'] ) {
          $sql = "UPDATE ". TABLE_CUSTOMERS . "
                     SET customers_password = '%s'
                   WHERE customers_id = %d";
          _zencart_db_query( $sql, _zencart_encrypt_password( $edit['pass'] ), $user_edit->zencart_customers_id );
        }
      }
    break;
    case "delete":
      // if the user has an automatically generated Zen Cart account
      if( $user_edit->zencart_customers_id && $user_edit->zencart_customer_generated ) {
        _zencart_delete_zencart_customer( $user_edit->zencart_customers_id );
      }
    break;
  }
}


/**
 * Custom validation for user login form
 *
 */
function zencart_users_login_validate($form_id, $form_values, $form) {
  /*
  if (isset($form_values['name'])) {
    if ($name = db_result(db_query("SELECT name FROM {users} WHERE LOWER(mail) = LOWER('%s')", $form_values['name']))) {
      form_set_value($form['name'], $name);
    }
  }
  */
}

/**
 * Implementation of hook_auth
 */
function zencart_users_auth($username, $password, $server = "") {
  _zen_include_file( "admin/includes/functions/general.php" );
  _zen_include_file( "includes/functions/password_funcs.php" );
  $customer = NULL;
  
  if( variable_get('zen_customer_drupal_login', true) ) {
    // if the user is trying to login with a Drupal username, check if that user 
    // has a zen customer account attached
    if( !$server ) {
      if ($account = user_load(array('name' => $username, 'status' => 1))) {
        if( $account->zencart_customers_id ) {
          $check_customer_query = "SELECT customers_password
                                     FROM " . TABLE_CUSTOMERS . "
                                    WHERE customers_id = '%d'";
          
          $customer = db_fetch_object( _zencart_db_query( $check_customer_query, $account->zencart_customers_id ) );
        }
      }
      
    }
    // if logging in with an email, check the zen cart db for that email
    else {  
      $check_customer_query = "SELECT customers_password
                                 FROM " . TABLE_CUSTOMERS . "
                                WHERE customers_email_address = '%s'";
      
      $customer = db_fetch_object( _zencart_db_query( $check_customer_query, $username . "@" . $server ) );
    }
    if ($customer && zen_validate_password($password, $customer->customers_password)) {
      return true;
    }
  }
  return false;
}

/**
 * Generate a Drupal Username for a Zen User
 */
function _zencart_users_get_username( $customers_firstname, $customers_lastname, $customers_email ) {
  $name  = $customers_firstname.$customers_lastname;
  if( $name ) {
    for( $i = 0; $i < 10; $i++ ) {
      $try_name = $name . ($i?$i:"");
      if( _zencart_users_name_is_valid( $try_name ) ) {
        return $try_name;
      }
    }
  } 
  
  // if we cannot find a legitimate name, just use the email address with @ and . removed
  $try_name = strtr($customers_email, "@.", "--");
  if( _zencart_users_name_is_valid( $try_name )  ) {
    return $try_name;
  }
  // if all else fails, use the email address
  return $customers_email;
}

/**
 * Check if a name is valid and available
 */
function _zencart_users_name_is_valid( $try_name ) {
  $valid = user_validate_name( $try_name ) == NULL;
  $taken = (db_num_rows(db_query("SELECT uid FROM {users} WHERE uid != %d AND LOWER(name) = LOWER('%s')", $uid, $try_name)) > 0);
  return( $valid && !$taken );
}

/**
 * Login a user in the Zen Cart install
 */
function _zencart_users_login_to_zencart($customers_id) {
  // already logged in to Zen Cart
  
  if( _zencart_session_read( "customer_id" ) == $customers_id ) {
    return;
  }
  
  $check_customer_query = "SELECT customers_id, customers_firstname, customers_default_address_id,
                                  customers_authorization
                             FROM " . TABLE_CUSTOMERS . "
                            WHERE customers_id = '%d'";
  $customer = db_fetch_object( _zencart_db_query( $check_customer_query, $customers_id ));
  if( !@$customer->customers_id ) {
    return false;
  }
  $check_country_query = "SELECT entry_country_id, entry_zone_id
                          FROM " . TABLE_ADDRESS_BOOK . "
                          WHERE customers_id = %d
                          AND address_book_id = %d";
  $check_country = db_fetch_object( _zencart_db_query( $check_country_query, $customer->customers_id, $customer->customers_default_address_id ) );
  
  $sess = array();
  $sess['customer_id']                  = $customer->customers_id;
  $sess['customer_default_address_id']  = $customer->customers_default_address_id;
  $sess['customers_authorization']      = $customer->customers_authorization;
  $sess['customer_first_name']          = $customer->customers_firstname;
  $sess['customer_country_id']          = @$check_country->entry_country_id;
  $sess['customer_zone_id']             = @$check_country->entry_zone_id;

  _zencart_session_write( $sess );
  $sql = "UPDATE " . TABLE_CUSTOMERS_INFO . "
          SET customers_info_date_of_last_logon = now(),
              customers_info_number_of_logons = customers_info_number_of_logons+1
          WHERE customers_info_id = %d";
//  _zencart_db_query( $sql, $_SESSION['customer_id'] );
//  var_dump( $sess ); die;

  // restore cart contents
  //$_SESSION['cart']->restore_contents();
  return true;
}

/**
 * Create a customer with the given credentials. If a user exists with the given
 * email address we simply update the password.
 * Returns the customers_id for the customer
 */
function _zencart_create_zencart_customer( $email, $password ) {
  $password_encrypted = _zencart_encrypt_password( $password );
  $customer = db_fetch_object( _zencart_db_query( "SELECT customers_id FROM " . TABLE_CUSTOMERS . " WHERE customers_email_address = '%s'", $email ) );
  if( $customer ) {
    _zencart_db_query( "UPDATE " . TABLE_CUSTOMERS . " SET customers_password = '%s' WHERE customers_id = '%s'", $password_encrypted, $customer->customers_id );
  } else {
    _zencart_db_query( "INSERT INTO " . TABLE_CUSTOMERS . " (customers_email_address, customers_password) VALUES ('%s','%s') ", $email, $password_encrypted );
    $customer = db_fetch_object( _zencart_db_query( "SELECT customers_id FROM " . TABLE_CUSTOMERS . " WHERE customers_email_address = '%s'", $email ) );
    if( @$customer->customers_id ) {
      _zencart_db_query( "INSERT INTO " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_date_of_last_logon, customers_info_date_account_created,customers_info_date_account_last_modified) VALUES (%d,NOW(),NOW(),NOW()) ", $customer->customers_id );
      return $customer->customers_id;
    }
  }
  watchdog('zencart_users', t('Unable to create Zen Cart Customer account for user withe email %email', array('%email' => $email)), WATCHDOG_ERROR);

}

/**
 * Delete a Zen Cart customer
 */
function _zencart_delete_zencart_customer( $customers_id ) {
  _zencart_db_query( "DELETE FROM " . TABLE_CUSTOMERS . " WHERE customers_id = '%s'", $customers_id );
  _zencart_db_query( "DELETE FROM " . TABLE_CUSTOMERS_INFO . " WHERE customers_info_id = '%s'", $customers_id );
  _zencart_db_query( "DELETE FROM " . TABLE_CUSTOMERS_BASKET . " WHERE customers_id = '%s'", $customers_id );
  _zencart_db_query( "DELETE FROM " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " WHERE customers_id = '%s'", $customers_id );
  _zencart_db_query( "DELETE FROM " . TABLE_CUSTOMERS_WISHLIST . " WHERE customers_id = '%s'", $customers_id );
}


function _zencart_encrypt_password( $pass ) {
  _zen_include_file( "admin/includes/functions/general.php" );
  _zen_include_file( "includes/functions/password_funcs.php" );
  
  return zen_encrypt_password( $pass );
}

function _zencart_session_read( $key=NULL ) {
  if( $session_key = _zencart_session_id( false ) ) {
    $session = db_fetch_object( 
      _zencart_db_query( "SELECT value
                          FROM " . TABLE_SESSIONS . "
                          WHERE sesskey = '%s'
                          AND expiry > %d", _zencart_session_id() , time()
                )
              );
    if( $session && $session->value && $session_array = _zencart_session_unserialize( $session->value ) ) {
      if( $key === NULL ) {
        return $session_array;
      } else if( is_array( $key ) ) {
        $out = array();
        foreach( $key as $key_item ) {
          $out[$key_item] = @$session_array[$key_item];
        }
        return( $out );
      } else {
        return $session_array[$key];
      }
    }
  }
  return NULL;
}

function _zencart_session_id($create=true) {
  if( $_COOKIE['zenid'] ) {
    return $_COOKIE['zenid'];
  } else if( $create ) {
    $session_id = session_id();
    setcookie( 'zenid', $session_id, 0, "/", ini_get('session.cookie_domain') );
    $_COOKIE['zenid'] = $session_id;
    return $session_id;
  } else {
    return NULL;
  }
}


function _zencart_session_write( $values ) {
  $zen_session = _zencart_session_read();
  if( !is_array( $zen_session ) ) {
    $zen_session = _zencart_session_create();
  }
  foreach( $values as $key => $value ) {
    $zen_session[$key] = $value;
  }
  $zen_session_encoded = _zencart_session_serialize($zen_session);
  _zencart_db_query( "UPDATE " . TABLE_SESSIONS . "
                      SET value = '%s'
                      WHERE sesskey = '%s'", 
                     $zen_session_encoded, _zencart_session_id()  
                  );
}

// update the session expiry to prevent automatic logout
function _zencart_session_touch() {
  if( $session_id = _zencart_session_id( false )  ) {
    _zencart_db_query( "UPDATE " . TABLE_SESSIONS . "
                        SET expiry = '%s'
                        WHERE sesskey = '%s'", 
                       time()+_zencrart_get_session_life(),
                       $session_id
                    );
  }
}

function _zencart_session_destroy() {
  if( $session_key = _zencart_session_id( false ) ) {
    _zencart_db_query( "DELETE FROM " . TABLE_SESSIONS . " WHERE sesskey = '%s'", $session_key );
    setcookie( 'zenid', '', time() - 42000, '/', ini_get('session.cookie_domain'));
  }
}

function _zencart_session_create() {
  
  _zencart_session_destroy();
  $new_session = _zencart_session_id();
  _zencart_db_query( "INSERT INTO " . TABLE_SESSIONS . " (sesskey, expiry) VALUES ('%s',%d)", $new_session, time()+_zencrart_get_session_life() );
  return array();
}

function _zencrart_get_session_life() {
  $SESS_LIFE = get_cfg_var('session.gc_maxlifetime');
  $SESS_LIFE = $SESS_LIFE?$SESS_LIFE:1440;
  return $SESS_LIFE;
}

function _zencart_session_unserialize($data) {
   $vars=preg_split( '/([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)\|/',
             $data,-1,PREG_SPLIT_NO_EMPTY |                 
              PREG_SPLIT_DELIM_CAPTURE
             );
   for($i=0; $vars[$i]; $i++) {
       $result[$vars[$i++]]=unserialize($vars[$i]);     
   }
   return $result;
}

function _zencart_session_serialize($data) {
   $out = "";
   foreach( $data as $key => $value ) {
      $out .= $key . "|" . serialize( $value );
   }
   return $out;
}

