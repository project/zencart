
Drupal.zencart_catalog = {
  callbackURL : "",  
  autoAttach  : function( ctxt ) {
    ctxt = ctxt || document;

    if ( $("#edit-zencart-products-is-product").length && !$("#edit-zencart-products-is-product").attr("checked")) {
      // Disable input and hide its description.
      $("fieldset.zencart_catalog_product fieldset").hide();
    }
    
    $("#edit-zencart-products-is-product").bind("click", function() {
      if (!$("#edit-zencart-products-is-product").attr("checked")) {
        $("fieldset.zencart_catalog_product fieldset").slideUp('slow');
      }
      else {
        // Auto-alias unchecked; enable input.
        $("fieldset.zencart_catalog_product fieldset").slideDown('slow');
      }
    });
    
    if ( $("#edit-zencart-node-is-category").length && !$("#edit-zencart-node-is-category").attr("checked")) {
      // Disable input and hide its description.
      $("fieldset.zencart_catalog_category fieldset").hide();
    }
    
    $("#edit-zencart-node-is-category").bind("click", function() {
      if (!$("#edit-zencart-node-is-category").attr("checked")) {
        $("fieldset.zencart_catalog_category fieldset").slideUp('slow');
      }
      else {
        // Auto-alias unchecked; enable input.
        $("fieldset.zencart_catalog_category fieldset").slideDown('slow');
      }
    });

    
  }
}


// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.zencart_catalog.autoAttach);
}
