-------------------------------------------------------------------------------
Zencart Integration for Drupal 5.x
  by Ronan Dowling, Gorton Studios - ronan (at) gortonstudios (dot) com
-------------------------------------------------------------------------------
  
This module provides integration with the Zen Cart eCommerce package with Drupal

NOTE: This is a development release, inteded only for use by people with a solid
working knowledge of both Drupal and Zen Cart. Patches and bug reports are
highly encouraged. Use this module at your own risk.

This module was designed to support Zen Cart version 1.3.7. Any help testing and
updating theis module for Zen Cart 1.3.8 would be very much apprecieated.
  
-------------------------------------------------------------------------------
Installation
------------

1) Install Drupal
2) Install Zen Cart on the same server (you should be able to install it 
   anywhere you like, but my testing was all done with an installation at 
   sites/default/zencart). 
   You need to completely install Zen Cart, including the initial DB setup.
   You may use a different database or the same one as your Drupal 
   install using table prefixes. I've mostly tested using seperate databases, 
   but in theory it shouldn't matter.
3) Install this module like any other Drupal module.
4) Install the Zen Cart component by copying the contents of the zencart 
   directory to your zencart directory. This should be a familiar if you've 
   installed Zen Cart contribution before. If you haven't then this module may
   not be for you.
5) Configure the Drupal module at admin/settings/zencart.
6) Test, patch and contribute back your fixes.

This module does not currently provide theme integration of any kind, so you 
will need to either create a Zen Cart theme that looks just like your Drupal 
theme, or come up with some cool and clever way to use your Drupal theme in 
Zen Cart (or vice versa) and then, of course, contribute it back.

-------------------------------------------------------------------------------
------------


